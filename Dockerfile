FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/socketio-channel

WORKDIR /home/node/socketio-channel

# Install dependencies
RUN npm install
RUN npm install pm2@2.6.1 -g

EXPOSE 8080
CMD pm2-docker --json app.yml